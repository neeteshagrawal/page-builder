import grapesjs from 'grapesjs';

  export default grapesjs.plugins.add("gjs-left-panel-component", (editor, opts = {}) => {

  const config = {
    blocks: ['emptyrow', 'emptycolumn', 'emptytab', 'object2', 'object3', 'object4', 'object5', 'object6'/*, 'object8','object10'*/],
    flexGrid: 0,
    stylePrefix: 'gjs-',
    addBasicStyle: true,
    category: 'Layout',
    labelEmptyRow: 'Empty Row',
    labelEmptyColumn: 'Empty Column',
    labelEmptyTab: 'Empty Tab',
    label2Object: '2 Object',
    label3Object: '3 Object',
    label4Object: '4 Object',
    label5Object: '5 Object',
    label6Object: 'More Object',
    ...opts
  }
  const loadBlocks = require('./blocks');
  loadBlocks.default(editor, config);
});


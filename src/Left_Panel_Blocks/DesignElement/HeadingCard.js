import React, { Component } from 'react';
import {ChromePicker}  from 'react-color';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import '../../Modal_Popups/Modal_Popus.css'
import '../../Modal_Popups/color-picker.css'
import ReactMde from "react-mde";
import * as Showdown from "showdown";
import "react-mde/lib/styles/css/react-mde-all.css";

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

const animatedComponents = makeAnimated();
const options = [
  { value: '101', label: 'Segment1' },
  { value: '102', label: 'Segment2' },
  { value: '103', label: 'Segment3' }
]

const MyComponent = () => (
  <Select  /*styles={customStyles}*/ options={options}  closeMenuOnSelect={true} components={animatedComponents} /*defaultValue={[options[1], options[2]]}*/ isMulti/>
)
class HeadingCard extends Component{
  
  constructor(props) {
  super(props);
  this.state={
    leftWidth:29,rightWidth:69,
   
    displayColorPicker :false,
    defaultColor:'#999',
    changeColor:'#999',
    //color:{r:'0',g:'9',b:'153',a:'1'},
    color:'#999',

    content_displayColorPicker :false,
    content_defaultColor:'#F39C12',
    content_changeColor:'#F39C12',
    content_color:{r:'0',g:'9',b:'153',a:'1'},

    main_displayColorPicker :false,
    main_defaultColor:'#8E44AD',
    main_changeColor:'#8E44AD',
    main_color:{r:'0',g:'9',b:'153',a:'1'},

    sub_displayColorPicker :false,
    sub_defaultColor:'#3498DB',
    sub_changeColor:'#3498DB',
    sub_color:{r:'0',g:'9',b:'153',a:'1'},

    selectedFile: null,
    imagePreviewUrl: null,
    isNormalLocation: true,
      isPersonalizedLocation: false,
      isSetting: true,
      istoggleFirstHeading: true,
      isLayout: true
  }

  }
  // image uploader START
  fileChangedHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
    let reader = new FileReader();
    reader.onloadend = () => {
      this.setState({
        imagePreviewUrl: reader.result
      });
    }
    reader.readAsDataURL(event.target.files[0])
  }
 
  submit = () => {
    var fd = new FormData();
    fd.append('file', this.state.selectedFile);
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        alert('Uploaded!');
      }
    };
    request.open("POST", "https://us-central1-tutorial-e6ea7.cloudfunctions.net/fileUpload", true);
    request.send(fd);
  }
  // image uploader END
  
  onValueChange=(event)=> {
    this.setState({
      leftWidth:(event.target.value),rightWidth:(98-event.target.value)
    });
  }
  onValueChangeFrame=(event)=> {
    this.setState({
     // leftWidth:(event.target.value),rightWidth:(98-event.target.value)
      //selectedOption: event.target.value
    });
   // alert(event.target.value);
  }
  drawDesgin()
  {
      this.props.designData('<div data-gjs-type="default" draggable="true"  class="gjs-row" ><div data-gjs-type="default" draggable="true" class="gjs-cell"></div></div>');
      this.props.onClose(false);
  }
  closeThis()
  {
      this.props.onClose(false);
  }
  onHandleShowColorPicker=(e)=>
  {
    var boxId=e.target.id;
    if(this.state.content_defaultColor){this.setState({content_displayColorPicker:false});}
    if(this.state.sub_displayColorPicker){this.setState({sub_displayColorPicker:false});}
    if(this.state.main_displayColorPicker){this.setState({main_displayColorPicker:false});}
    if(this.state.displayColorPicker){this.setState({displayColorPicker:false});}
    if(boxId==="clr-pkr-content-color"){ this.setState({content_displayColorPicker:true});}
    else if(boxId==="clr-pkr-color"){ this.setState({displayColorPicker:true});}
    else if(boxId==="clr-pkr-main-color"){ this.setState({main_displayColorPicker:true});}
    else if(boxId==="clr-pkr-sub-color"){ this.setState({sub_displayColorPicker:true});}
  }
  onChangeColorPicker=(color, event) =>{
    this.setState({color:color.rgb,changeColor:color.hex});
  }
  onChangeMainColorPicker=(color, event) =>{
    this.setState({main_color:color.rgb,main_changeColor:color.hex});
  }
  onChangeContentColorPicker=(color, event) =>{
    this.setState({content_color:color.rgb,content_changeColor:color.hex});
  }
  onChangeSubColorPicker=(color, event) =>{
    this.setState({sub_color:color.rgb,sub_changeColor:color.hex});
  }

  handleClose = (e) => {
    if(this.state.content_displayColorPicker){this.setState({content_displayColorPicker:false});}
    if(this.state.sub_displayColorPicker){this.setState({sub_displayColorPicker:false});}
    if(this.state.main_displayColorPicker){this.setState({main_displayColorPicker:false});}
    if(this.state.displayColorPicker){this.setState({displayColorPicker:false});}
  };
  showNormalLocation = (e) => {
    this.setState(
      {
        isNormalLocation: true,
        isPersonalizedLocation: false,
        isDividerTemplate:false,
     },
      () => {
        console.log(this.state);
      }
    );
  };
  showPersonalizedLocation = (e) => {
    this.setState(
      {
        isNormalLocation: false,
        isPersonalizedLocation: true,
        isDividerTemplate:false
      },
      () => {
        console.log(this.state);
      }
    );
  };
  showSetting = (e) => {
    this.setState({ isSetting: false }, () => {
      console.log(this.state)
    });
  };
  upSetting = (e) => {
    this.setState({ isSetting: true }, () => {
      console.log(this.state)
    });
  };

  toggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: false }, () => {
    });
  };
  uptoggleFirstHeading = (e) => {
    this.setState({ istoggleFirstHeading: true }, () => {
    });
  };
  downLayout = (e) => {
    this.setState({ isLayout: false }, () => {
    });
  };
  upLayout = (e) => {
    this.setState({ isLayout: true }, () => {
    });
  };

  render() 
  {
    let $imagePreview = (<div className="previewText image-container">Please select an Image for Preview</div>);
    if (this.state.imagePreviewUrl) {
      $imagePreview = (<div className="image-container" ><img src={this.state.imagePreviewUrl} alt="icon" width="200" /> </div>);
    }
    var lytImg=<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="254.000000pt" height="80px" viewBox="0 0 254.000000 429.000000" preserveAspectRatio="xMidYMid meet"> <metadata> Created by potrace 1.16, written by Peter Selinger 2001-2019 </metadata> <g transform="translate(0.000000,429.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"> <path d="M161 4279 c-38 -8 -92 -48 -123 -91 l-23 -33 -3 -2003 c-2 -1985 -2 -2003 18 -2042 11 -21 36 -51 57 -67 l36 -28 1134 -3 c1275 -3 1177 -8 1234 74 l29 43 0 2012 c0 1851 -1 2014 -16 2038 -26 41 -56 72 -89 89 -27 16 -125 17 -1127 17 -603 0 -1110 -3 -1127 -6z m2193 -139 c26 -9 26 -11 25 -117 0 -60 -4 -116 -8 -126 -6 -16 -9 -16 -31 8 l-23 25 -1053 0 -1053 0 -15 -22 c-14 -20 -16 -93 -16 -592 0 -516 2 -571 17 -588 15 -17 62 -18 1068 -18 1036 0 1053 0 1078 20 22 17 26 18 31 4 3 -9 6 -550 6 -1203 0 -910 -3 -1190 -12 -1199 -9 -9 -267 -12 -1099 -12 -1007 0 -1089 1 -1104 17 -16 16 -17 142 -12 1901 3 1037 10 1889 14 1893 5 5 348 10 763 12 415 1 900 3 1077 5 191 1 332 -2 347 -8z m-675 -679 c59 -31 65 -42 63 -138 l-1 -86 -42 -38 c-24 -22 -51 -39 -61 -39 -9 0 -31 15 -47 32 -27 29 -29 34 -16 48 13 12 19 12 35 2 29 -18 50 -15 50 8 0 23 -10 26 -29 8 -11 -11 -15 -10 -19 2 -3 8 -21 29 -40 47 -58 53 -43 125 33 159 17 7 31 13 33 14 1 0 20 -9 41 -19z m-781 -13 c4 -13 7 -74 6 -136 -2 -120 -10 -146 -42 -126 -13 8 -18 30 -22 85 -3 41 -11 85 -18 97 -11 19 -11 30 -2 53 21 50 65 65 78 27z m244 5 c10 -9 18 -22 18 -29 0 -18 -36 -27 -59 -15 -31 17 -41 13 -41 -16 0 -41 15 -60 34 -44 28 23 86 -28 86 -76 0 -22 -41 -91 -63 -105 -41 -26 -120 38 -133 107 -15 79 16 175 56 175 10 0 22 5 25 10 10 16 57 12 77 -7z"/> </g> </svg>;
    var biImg = <svg class="bi bi-chevron-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z"></path> </svg>;
    var downImg =
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
      </svg>
   return(<>
  <div className="row" style={{marginBottom:"10px"}}>
  <div className="left-side">
       
        </div>
        <div className="right-side">
        <div className="d-flex" style={{marginTop:"5px"}}>
              {/* <input type="button"/>
              <input type="button"/>
              <input type="button"/> */}
                <span className="fa fa-desktop btn-device-preview"></span>
                <span className="fa fa-tablet btn-device-preview"></span>
                <span className="fa fa-mobile btn-device-preview"></span>
              <span style={{textAlign:"right",width:"100%"}}>
                <input type="button" value="Preview Changes" className="btn-preview-changes"/></span>
        </div>
        
        </div>
</div>

    <div className="row">
<div className="left-side">
{this.state.isLayout === true ?
            <div className="Layout1" >
              <div className="heading-penal" onClick={this.downLayout}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {biImg}
                </span>
                <h3 className="margin0" style={{ textAlign: "left" }}>Layout </h3>
              </div>
              <div className="card">
                <h4 style={{ textAlign: "left" }} className="margin0">Heading Card Layout</h4>
                <div className="row">
                  <p style={{ textAlign: "left" }}>Static Heading Card </p>
                  <div className="gjs-pn-panel-banner">
                    <label> <input type="radio" name="Layout" value="29" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="69" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="39" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="59" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="49" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="09" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="19" onChange={this.onValueChange} /> {lytImg}  </label>
                    <label> <input type="radio" name="Layout" value="79" onChange={this.onValueChange} /> {lytImg}  </label>
                  </div>
                </div>
                <div className="row">
                  <p style={{ textAlign: "left" }} className="">Static Heading Card </p>
                  <div className="gjs-pn-panel-banner">
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                    <label> <input type="radio" name="" /> {lytImg}  </label>
                  </div>
                </div>
              </div>
              <div className="card">
                <h4 style={{ textAlign: "left" }} className="margin0">Frame style</h4>
                <div className="row frame-style">
                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Rounded Edges</span>
                    <div className="color-picker-container">
                      <input id="rs-range-line" className="rs-range frame-style-input" type="range" onChange={this.onValueChangeFrame} />
                    </div></div>
                  <div className="d-flex"> <span className="slide-conten" style={{ marginRight: "10px" }}>Overlay Color</span>
                    <div className="switch">
                      <div className="checkbox switcher">
                        <label htmlFor="overlayColor"><input type="checkbox" id="overlayColor" onChange={this.onValueChangeFrame} value="" /><span><small></small></span> </label>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Opacity</span>
                    <div className="color-picker-container">
                      <input id="rs-range-line1" className="rs-range frame-style-input" type="range" onChange={this.onValueChangeFrame} />
                    </div></div>


                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Color</span>
                    <div className="color-picker-container">
                      <div className="color-picker-color-background" style={{ backgroundColor: this.state.changeColor }}></div>
                      <input type="text" id="clr-pkr-color" readOnly value={this.state.changeColor} className="color-picker-text" onClick={this.onHandleShowColorPicker} />
                      {this.state.displayColorPicker && (
                        <div className="color-picker-palette">
                          <div className="color-picker-cover" onClick={this.handleClose}> </div>
                          <ChromePicker color={this.state.color} onChange={(color, evt) => this.onChangeColorPicker(color, evt)}></ChromePicker>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="d-flex"> <span className="slide-conten" style={{ marginRight: "10px" }}>Content Margin</span>
                    <div className="color-picker-container">
                      <input type="text" style={{ paddingRight: "35px" }} defaultValue="1px" className="frame-style-input" />
                    </div></div>
                  <div className="d-flex"> <span className="slide-conten" style={{ marginRight: "10px" }}>Element Color</span>
                    <div className="switch">
                      <div className="checkbox switcher">
                        <label htmlFor="elementColor"><input type="checkbox" id="elementColor" onChange={this.onValueChangeFrame} value="" /><span><small></small></span> </label>
                      </div>
                    </div>
                  </div>
                  {/* <div className="d-flex"> 
      <span className="slide-conten" style={{marginRight: "10px"}}>Color</span>
      <input id="clr-pkr-color" className="color-picker-text"  onClick={this.onHandleShowColorPicker} />
      </div> */}
                  {/* <div className="d-flex"> 
      <span className="slide-conten" style={{marginRight: "10px"}}>Content Color</span>
      <input id="clr-pkr-content-color" className="color-picker-text" onClick={this.onHandleShowColorPicker}/>
      </div> */}
                  {/* <div className="d-flex"> 
      <span className="slide-conten" style={{marginRight: "10px"}}>Main Color</span>
      <input id="clr-pkr-main-color" className="color-picker-text" onClick={this.onHandleShowColorPicker}/>
      </div> */}
                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Content Color</span>
                    <div className="color-picker-container">
                      <div className="color-picker-color-background" style={{ backgroundColor: this.state.content_changeColor }}></div>
                      <input type="text" id="clr-pkr-content-color" readOnly value={this.state.content_changeColor} className="color-picker-text" onClick={this.onHandleShowColorPicker} />
                      {this.state.content_displayColorPicker && (
                        <div className="color-picker-palette">
                          <div className="color-picker-cover" onClick={this.handleClose}> </div>
                          <ChromePicker color={this.state.content_color} onChange={this.onChangeContentColorPicker}></ChromePicker>
                        </div>
                      )}
                    </div>
                  </div>

                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Main Color</span>
                    <div className="color-picker-container">
                      <div className="color-picker-color-background" style={{ backgroundColor: this.state.main_changeColor }}></div>
                      <input type="text" id="clr-pkr-main-color" readOnly value={this.state.main_changeColor} className="color-picker-text" onClick={this.onHandleShowColorPicker} />
                      {this.state.main_displayColorPicker && (
                        <div className="color-picker-palette">
                          <div className="color-picker-cover" onClick={this.handleClose}> </div>
                          <ChromePicker color={this.state.main_color} onChange={this.onChangeMainColorPicker}></ChromePicker>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="d-flex">
                    <span className="slide-conten" style={{ marginRight: "10px" }}>Sub Color</span>
                    <div className="color-picker-container">
                      <div className="color-picker-color-background" style={{ backgroundColor: this.state.sub_changeColor }}></div>
                      <input type="text" id="clr-pkr-sub-color" readOnly value={this.state.sub_changeColor} className="color-picker-text" onClick={this.onHandleShowColorPicker} />
                      {this.state.sub_displayColorPicker && (
                        <div className="color-picker-palette">
                          <div className="color-picker-cover" onClick={this.handleClose}> </div>
                          <ChromePicker color={this.state.sub_color} onChange={this.onChangeSubColorPicker}></ChromePicker>
                        </div>
                      )}
                    </div>
                  </div>


                  {/* <div className="d-flex"> <span className="slide-conten">opacity</span>
          <div className="switch">
            <div className="checkbox switcher">
              <label htmlFor="test"><input type="checkbox" id="test" onChange={this.onValueChangeFrame} value=""/> <span><small></small></span> </label>
            </div>
          </div>
        </div> */}
                </div>
              </div>
            </div> :
            <div className="heading-penal" onClick={this.upLayout}>
              <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                {downImg}
              </span>
              <h3 className="margin0" style={{ textAlign: "left" }}>Layout </h3>
            </div>}
          <br />

          {this.state.isSetting === true ? 
                      <div className="banner-conten" >
                      <div className="heading-penal" onClick={this.showSetting}>
                        <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                          {biImg}
                        </span>
                        <h3 className="">Setting</h3>
                      </div>
        
  <div className="card pt-0">
              <div className="justify-content-around position-relative overflow-hidden ">
                <h3 className="mt-0"> Heading Text Setting</h3>
                </div>
              <ul>
                <li>
                  <strong>Font
                    <ul>
                      <li>
                        <label>Arabic:
                          <ul>
                            <li><br/>
                              Font:
                              
                <select name="cars" id="cars" style={{width: "117px",marginLeft: "45px"}}>  </select>
                <br/></li>
                            <li>
                            <br/> Font  Type:
                <select name="cars" id="cars" style={{width: "117px",marginLeft: "5px"}}>  </select>
                <br/> </li>
                            
                          </ul>
                        </label><br/>
                      </li>
                      <li>
                        <label>English:
                        <ul>
                            <li>
                            <br/> Font:
                              
                <select name="cars" id="cars" style={{width: "117px",marginLeft: "45px"}}>  </select>
                <br/></li>
                            <li>
                            <br/>  Font  Type:
                <select name="cars" id="cars" style={{width: "117px",marginLeft: "5px"}}>  </select>
                <br/>       </li>
                            
                          </ul>
                        </label>
                      </li>
                    </ul>
                  </strong>
                </li>
                <li><strong><br/>Font Size
                <ul>
                      <li>
                        <label>Arabic:
                          <ul>
                            <li>
                              Font Size:
                              <input type="range" min="1" max="100" value="50" style={{marginLeft: "5px"}}/>
                               <br/></li>
                          </ul>
                        </label><br/>
                      </li>
                      <li>
                        <label>English:
                        <ul>
                            <li>
                            <br/> Font Size:
                            <input type="range" min="1" max="100" value="50"/>
                <select name="cars" id="cars" style={{marginLeft: "5px"}}>  </select>
                <br/></li>
                            
                          </ul>
                        </label>
                      </li>
                    </ul></strong></li>
              </ul>
             
          </div> </div> : <div className="heading-penal" onClick={this.upSetting}>
              <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                {downImg}
              </span>
              <h3 className="">Setting</h3>
            </div>}
          <br/>

<div className="banner-conten">
{this.state.istoggleFirstHeading === true ? <div>
              <div className="heading-penal" onClick={this.toggleFirstHeading}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {biImg}
                </span>
                <h3 className="">Heading Text Content</h3>
              </div>

  <div className="add-btn"> 
  {/* <input type="file" name="avatar" onChange={this.fileChangedHandler} />
         <button type="button" onClick={this.submit} className="btn"> Upload </button>
         { $imagePreview } */}
    <button className="btn" onClick={this.showNormalLocation}> + Add New Heading Text </button>
    <button className="btn" onClick={this.showPersonalizedLocation}> + Add Personalized Heading Text </button>
   
  </div>
  {this.state.isNormalLocation === true ? (
                <div className="card pt-0">
                  <div className="justify-content-around position-relative overflow-hidden ">
                    <h3 className="mt-0">Headings Text 1</h3>
                    <div className="personlized"> Normal Text</div>
                  </div>

                  <div className="d-flex">
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                      Headings Text Name&nbsp;&nbsp;&nbsp;
                    </span>
                    <div className="color-picker-container">
                      <input type="text" className="frame-style-input" />
                    </div>
                  </div>
                 <h3> Arabic Text</h3>
                  <div className="d-flex" style={{border: "1px solid grey",paddingLeft: "-48px",marginLeft: "-10px"}}>
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                    </span>
                   
                    <ReactMde
        // value={value}
        // onChange={setValue}
        // selectedTab={selectedTab}
        // onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
      />
                  </div>
                  <h3> English Text</h3>
                  <div className="d-flex" style={{border: "1px solid grey",paddingLeft: "-48px",marginLeft: "-10px"}}>
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                    </span>
                    <ReactMde
        // value={value}
        // onChange={setValue}
        // selectedTab={selectedTab}
        // onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
      />
                  </div>
                </div>
              ) : (
                ""
              )}
                {this.state.isPersonalizedLocation === true ? (
                <div className="card pt-0">
                  <div className="justify-content-around position-relative overflow-hidden ">
                    <h3 className="mt-0">Headings Text 2</h3>
                    <div className="personlized"> Personlized Text</div>
                  </div>

                  <div className="d-flex">
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                      Headings Text Name&nbsp;&nbsp;&nbsp;
                    </span>
                    <div className="color-picker-container">
                      <input type="text" className="frame-style-input" />
                    </div>
                  </div><br/>
                 
                <div className="d-flex">
                    <span
                      className="slide-conten"
                      style={{ marginRight: "104px" }}
                    >
                      Segment & Tags
                    </span>
                    <div className="color-picker-container">
                      <MyComponent />
                      <p className="mt-0">
                        <small>
                          select the segment of contact that will see this
                          banner(you can select and add more than one)
                        </small>
                      </p>
                    </div>
                  </div>
                  <h3> Arabic Text</h3>
                  <div className="d-flex" style={{border: "1px solid grey",paddingLeft: "-48px",marginLeft: "-10px"}}>
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                    </span>
                    <ReactMde
        // value={value}
        // onChange={setValue}
        // selectedTab={selectedTab}
        // onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
      />
                  </div>
                  <h3> English Text</h3>
                  <div className="d-flex" style={{border: "1px solid grey",paddingLeft: "-48px",marginLeft: "-10px"}}>
                    {" "}
                    <span
                      className="slide-conten"
                      style={{ marginRight: "10px" }}
                    >
                    </span>
                    
                    <ReactMde
        // value={value}
        // onChange={setValue}
        // selectedTab={selectedTab}
        // onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
      />
                  </div>
                </div>
              ) : (
                ""
              )}
              </div>
              :
              <div className="heading-penal" onClick={this.uptoggleFirstHeading}>
                <span class=" float-right" style={{ color: "#fff", fontSize: "20px" }}>
                  {downImg}
                </span>
                <h3 className="">Heading Text  Content</h3>
              </div>}

</div>
</div>

</div>
</>
);
  }

}
export default HeadingCard;

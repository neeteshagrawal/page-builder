export default function (editor, opt = {}) {
  const c = opt;
  let bm = editor.BlockManager;
  let blocks = c.blocks;
  let stylePrefix = c.stylePrefix;
  const flexGrid = c.flexGrid;
  const basicStyle = c.addBasicStyle;
  const clsRow = `${stylePrefix}row`;
  const clsCell = `${stylePrefix}cell`;
  const styleRow = flexGrid ? `
    .${clsRow} {
      display: flex;
      justify-content: flex-start;
      align-items: stretch;
      flex-wrap: nowrap;
      padding: 10px;
    }
    @media (max-width: 768px) {
      .${clsRow} {
        flex-wrap: wrap;
      }
    }` : `
    .${clsRow} {
      display: table;
      padding: 10px;
      width: 100%;
    }
    @media (max-width: 768px) {
      .${stylePrefix}cell, .${stylePrefix}cell30, .${stylePrefix}cell70 {
        width: 100%;
        display: block;
      }
    }`;
  const styleClm = flexGrid ? `
    .${clsCell} {
      min-height: 75px;
      flex-grow: 1;
      flex-basis: 100%;
    }` : `
    .${clsCell} {
      width: 8%;
      display: table-cell;
      height: 75px;
    }`;
  const styleClm30 = `
  .${stylePrefix}cell30 {
    width: 30%;
  }`;
  const styleClm70 = `
  .${stylePrefix}cell70 {
    width: 70%;
  }`;

  const step = 0.2;
  const minDim = 1;
  const currentUnit = 1;
  const resizerBtm = { tl: 0, tc: 0, tr: 0, cl: 0, cr:0, bl:0, br: 0, minDim };
  const resizerRight = { ...resizerBtm, cr: 1, bc: 0, currentUnit, minDim, step };

  // Flex elements do not react on width style change therefore I use
  // 'flex-basis' as keyWidth for the resizer on columns
  if (flexGrid) {
    resizerRight.keyWidth = 'flex-basis';
  }

  const rowAttr = {
    class: clsRow,
    'data-gjs-droppable': `.${clsCell}`,
    'data-gjs-resizable': resizerBtm,
    'data-gjs-name': 'Row',
  };

  const colAttr = {
    class: clsCell,
    'data-gjs-draggable': `.${clsRow}`,
    'data-gjs-resizable': resizerRight,
    'data-gjs-name': 'Cell',
  };

  if (flexGrid) {
    colAttr['data-gjs-unstylable'] = ['width'];
    colAttr['data-gjs-stylable-require'] = ['flex-basis'];
  }

  // Make row and column classes private
  const privateCls = [`.${clsRow}`, `.${clsCell}`];
  editor.on('selector:add', selector =>
    privateCls.indexOf(selector.getFullName()) >= 0 && selector.set('private', 1))

  const attrsToString = attrs => {
    const result = [];

    for (let key in attrs) {
      let value = attrs[key];
      const toParse = value instanceof Array || value instanceof Object;
      value = toParse ? JSON.stringify(value) : value;
      result.push(`${key}=${toParse ? `'${value}'` : `"${value}"`}`);
    }

    return result.length ? ` ${result.join(' ')}` : '';
  }

  const toAdd = name => blocks.indexOf(name) >= 0;
  const attrsRow = attrsToString(rowAttr);
  const attrsCell = attrsToString(colAttr);

  toAdd('text') && bm.add('text', {
    label: c.labelText,
    category: c.category,
    attributes: {class:'fa fa-align-left'},
    type:'text',
    content:  {type:'text'}
  });
  toAdd('heading') && bm.add('heading', {
    label: c.labelHeading,
    category: c.category,
    type:'heading',
     content:{type:'heading'},
     attributes: {class:'fa fa-text-width'}
  });
 
  toAdd('button') && bm.add('button', {
    label: c.labelButton,
    attributes: {class:'fa fa-hand-pointer-o'},
    category: c.category,
    content: `<div ${attrsRow}>
        <div ${attrsCell}></div>
        <div ${attrsCell}></div>
      </div>
      ${ basicStyle ?
        `<style>
          ${styleRow}
          ${styleClm}
        </style>`
        : ''}`
  });

  toAdd('divider') && bm.add('divider', {
    label: c.labelDivider,
    category: c.category,
    attributes: {class:'fa fa-exchange'},
    type:'divider',
    content: {type:'divider'}
  });

  toAdd('tabCard') && bm.add('tabCard', {
    label: c.labelTab,
    category: c.category,
    attributes: {class:'fa fa-external-link'},
    type:'tabCard',
    content: {type:'tabCard'}
  });

  toAdd('htmlcode') && bm.add('htmlcode', {
    label: c.labelHTMLCode,
    category: c.category,
    attributes: {class:'fa fa-code'},
    type:'htmlcode',
    content:{type:'htmlcode'}
  });
  // toAdd('htmlcode') && bm.add('htmlcode', {
  //   label: c.labelHTMLCode,
  //   category: c.category,
  //   type:'htmlcode',
  //   content:{type:'htmlcode'},
  //  attributes: {class:'fa fa-code'}
 
  // });
  
  // toAdd('htmlcode') && bm.add('htmlcode', {
  //   label: c.labelHTMLCode,
  //   category: c.category,
  //   type:'htmlcode',
  //    attributes: {class:'fa fa-image'},
  //   content:{type:'htmlcode'}
  //   // attributes: {class:'fa fa-code'},
  //   // content: `<div ${attrsRow}>
  //   //     <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 30%;"></div>
  //   //     <div ${attrsCell} style="${flexGrid ? 'flex-basis' : 'width'}: 70%;"></div>
  //   //   </div>
  //   //   ${ basicStyle ?
  //   //     `<style>
  //   //       ${styleRow}
  //   //       ${styleClm}
  //   //       ${styleClm30}
  //   //       ${styleClm70}
  //   //     </style>`
  //   //     : ''}`,
  //   //     type:'script'
  // });

  toAdd('accordion') && bm.add('accordion', {
    label: c.labelAccordion,
    category: c.category,
    type:'uxaccordion',
    content:{type:'uxaccordion'},

    attributes: {class:'fa fa-archive'},
    //type:'accordions',
    //content: '<div data-gjs-type="accordions"></div><style>.accordion{text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:inherit;padding-top:7px;padding-right:14px;padding-bottom:7px;padding-left:14px;transition-duration:.3s;transition-timing-function:ease;transition-delay:0s;transition-property:opacity;display:block;border-top-left-radius:3px;border-top-right-radius:3px;border-bottom-right-radius:3px;border-bottom-left-radius:3px;margin-right:10px;background-color:#eee;margin-top:5px}.accordion-content{display:none;padding-top:6px;padding-right:12px;padding-bottom:6px;padding-left:12px;min-height:100px;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-top-color:#eee;border-right-color:#eee;border-bottom-color:#eee;border-left-color:#eee;border-image-source:initial;border-image-slice:initial;border-image-width:initial;border-image-outset:initial;border-image-repeat:initial}</style>'
    //accordions
  });

  toAdd('searchbar') && bm.add('searchbar', {
    label: c.labelSearchBar,
    category: c.category,
    attributes: {class:'fa fa-search'},
    content: {
      type:'link',
      content:'Link',
      style: {color: '#d983a6'}
    },
  });

  toAdd('megamenu') && bm.add('megamenu', {
    label: c.labelMegaMenu,
    category: c.category,
    attributes: {class:'fa fa-bars'},
    // content: {
    //   style: {color: 'black'},
    //   type:'image',
    //   activeOnRender: 1
    // },
  });

 
  toAdd('object8') && bm.add('object8', {
    label: c.label8Object,
    category: c.category,
    attributes: {class:'fa fa-youtube-play'},
    content: {
      type: 'video',
      src: 'img/video2.webm',
      style: {
        height: '350px',
        width: '615px',
      }
    },
  });

  toAdd('object10') && bm.add('object10', {
    label: c.label10Object,
    category: c.category,
    attributes: {class:'fa fa-map-o'},
    content: {
      type: 'map',
      style: {height: '350px'}
    },
  });
}

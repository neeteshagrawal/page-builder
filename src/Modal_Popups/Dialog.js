import React,{Component} from 'react';
class Dialog extends Component{
    render()
    {
        let dialog=(  
        <div className="gjs-mdl-container">
        <div className="gjs-mdl-dialog gjs-one-bg gjs-two-color">
        <div className="gjs-mdl-header">
        <div className="gjs-mdl-title">{this.props.cardName}</div>
        <div className="gjs-mdl-btn-close" onClick={this.props.onClose}>⨯</div>
        </div>
        <div className="gjs-mdl-content"> {this.props.children}</div>
        </div></div>)
        if(! this.props.isOpen)
        {dialog=null}

    return (<div>{dialog}</div>)
    }
}
export default Dialog;
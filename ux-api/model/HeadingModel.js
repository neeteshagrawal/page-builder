var mongoose = require('mongoose');
// Setup schema
var HeadingSchema = mongoose.Schema({

    text_name: {
        type: String
        
    },
    arabic_text: {
        type: String
        
    },
    english_text :{
        type: String
    },
    segment_tag :{
        type: String
    }

   
});
// Export Contact model
var Headings = module.exports = mongoose.model('headings', HeadingSchema);
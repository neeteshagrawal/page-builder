var mongoose = require('mongoose');
// Setup schema
var TabSchema = mongoose.Schema({
    tab_name: {
        type: String
        
    },
    element_name: {
        type: String
        
    },
    tab_content :{
        type: String
    },
    segment_tag :{
        type: String
    }

   
});
// Export Contact model
var Tab = module.exports = mongoose.model('tabs', TabSchema);
var mongoose = require('mongoose');
// Setup schema
var accordionSettingSchema = mongoose.Schema({
    border: {
        type: String
        /*required: true*/
    },
    height: {
        type: String
        /*required: true*/
    },
    width: {
        type: String
       /* required: true*/
    },
    rounded_edge: {
        type: String
       /* required: true*/
    },
    accordian_icon: {
        type: String
       /* required: true*/
    },
    label: {
        type: String
       /* required: true*/
    },
    label_aligment: {
        type: String
       /* required: true*/
    },
    icon_position: {
        type: String
       /* required: true*/
    },
    active_accordion_clr: {
        type: String
       /* required: true*/
    }, 
    active_lbl_clr: {
        type: String
       /* required: true*/
    }, 
    inactive_accordion_clr: {
        type: String
       /* required: true*/
    }, 
    inactive_lbl_clr: {
        type: String
       /* required: true*/
    }, 
    border_clr: {
        type: String
       /* required: true*/
    },  
    background_clr: {
        type: String
       /* required: true*/
    } ,
    page_id: {
        type: String
       /* required: true*/
    }   

});
// Export Contact model
var Accordionsetting = module.exports = mongoose.model('accordionsettings', accordionSettingSchema);
var mongoose = require('mongoose');
// Setup schema
var videoSchema = mongoose.Schema({
    video_name: {
        type: String
        /*required: true*/
    },
    segment_tag: {
        type: String
        /*required: true*/
    },
    auto_play: {
        type: String
       /* required: true*/
    },
    controls: {
        type: String
       /* required: true*/
    },
    muted: {
        type: String
       /* required: true*/
    },
    video1: {
        type: String
       /* required: true*/
    },
    video2: {
        type: String
       /* required: true*/
    }    
});
// Export Contact model
var Video = module.exports = mongoose.model('videos', videoSchema);
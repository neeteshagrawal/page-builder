var mongoose = require('mongoose');
// Setup schema
var layoutSchema = mongoose.Schema({
    static_banner: {
        type: String
        /*required: true*/
    },
    round_edge: {
        type: String
        /*required: true*/
    },
    overlay_color: {
        type: String
       /* required: true*/
    },
    opacity: {
        type: String
       /* required: true*/
    },
    color: {
        type: String
       /* required: true*/
    },
    content_margin: {
        type: String
       /* required: true*/
    },
    element_color: {
        type: String
       /* required: true*/
    },
    content_color: {
        type: String
       /* required: true*/
    }, 
    main_color: {
        type: String
       /* required: true*/
    }, 
    sub_color: {
        type: String
       /* required: true*/
    }
    ,
    page_id: {
        type: String
       /* required: true*/
    }   

});
// Export Contact model
var Layouts = module.exports = mongoose.model('layouts', layoutSchema);
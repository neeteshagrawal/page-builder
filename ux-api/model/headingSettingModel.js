var mongoose = require('mongoose');
// Setup schema
var headingSettingSchema = mongoose.Schema({

    english_font: {
        type: String        
    },
    arabic_font: {
        type: String        
    },
    english_font_type :{
        type: String
    },
    arabic_font_type :{
        type: String
    },
    english_font_size :{
        type: String
    },
    arabic_font_size :{
        type: String
    }   
});
// Export Contact model
var Headingsettings = module.exports = mongoose.model('headingsettings', headingSettingSchema);
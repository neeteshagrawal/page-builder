var mongoose = require('mongoose');
// Setup schema
var emptyColumnSchema = mongoose.Schema({
    page_id: {
        type: String
        /*required: true*/
    },
    column_number: {
        type: String
        /*required: true*/
    },
    order: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var EmptyColumn = module.exports = mongoose.model('emptyColumns', emptyColumnSchema);
var mongoose = require('mongoose');
// Setup schema
var dividerSettingSchema = mongoose.Schema({
    line_thickness: {
        type: String
        /*required: true*/
    },
    line_width: {
        type: String
        /*required: true*/
    },
    add_element: {
        type: String
       /* required: true*/
    },
    element_position: {
        type: String
       /* required: true*/
    },
    line_thickness_color: {
        type: String
       /* required: true*/
    },
    element_color: {
        type: String
       /* required: true*/
    },
    element_background_color: {
        type: String
       /* required: true*/
    },
   
    page_id: {
        type: String
       /* required: true*/
    }   

});
// Export Contact model
var Dividersetting = module.exports = mongoose.model('dividersettings', dividerSettingSchema);
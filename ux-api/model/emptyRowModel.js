var mongoose = require('mongoose');
// Setup schema
var emptyRowSchema = mongoose.Schema({
    page_id: {
        type: String
        /*required: true*/
    },
    empty_row: {
        type: String
        /*required: true*/
    },
    order: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var EmptyRow = module.exports = mongoose.model('emptyRows', emptyRowSchema);
var mongoose = require('mongoose');
// Setup schema
var categorySchema = mongoose.Schema({
    segment_tags: {
        type: String
        /*required: true*/
    },
    category_level: {
        type: String
        /*required: true*/
    },
    media_column: {
        type: String
       /* required: true*/
    },
    no_of_column: {
        type: String
       /* required: true*/
    },
     no_of_row: {
        type: String
       /* required: true*/
    },
     text_size: {
        type: String
       /* required: true*/
    },
     text_color: {
        type: String
       /* required: true*/
    },
     text_bgcolor: {
        type: String
       /* required: true*/
    }
    

});
// Export Contact model
var Category = module.exports = mongoose.model('categories', categorySchema);
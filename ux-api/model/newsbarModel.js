var mongoose = require('mongoose');
// Setup schema
var newsbarSchema = mongoose.Schema({
    news_speed: {
        type: String
        /*required: true*/
    },
    segment_tags: {
        type: String
        /*required: true*/
    },
    message_name: {
        type: String
       /* required: true*/
    },
    message_content1: {
        type: String
       /* required: true*/
    },
    message_content2: {
        type: String
       /* required: true*/
    }
      
});
// Export Contact model
var Newsbar = module.exports = mongoose.model('newsbars', newsbarSchema);
var mongoose = require('mongoose');
// Setup schema
var fourObjectSchema = mongoose.Schema({
    page_id: {
        type: String
        /*required: true*/
    },
    column_type: {
        type: String
        /*required: true*/
    },
    order: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var FourObject = module.exports = mongoose.model('fourObjects', fourObjectSchema);
var mongoose = require('mongoose');
// Setup schema
var twoObjectSchema = mongoose.Schema({
    page_id: {
        type: String
        /*required: true*/
    },
    column_type: {
        type: String
        /*required: true*/
    },
    order: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var TwoObject = module.exports = mongoose.model('twoObjects', twoObjectSchema);
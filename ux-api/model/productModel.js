var mongoose = require('mongoose');
// Setup schema
var productSchema = mongoose.Schema({
    sku: {
        type: String
        /*required: true*/
    },
    name: {
        type: String
        /*required: true*/
    },
    brand_name: {
        type: String
       /* required: true*/
    },
    price: {
        type: String
       /* required: true*/
    },
     qty: {
        type: String
       /* required: true*/
    },
     type: {
        type: String
       /* required: true*/
    },
     attribute_group: {
        type: String
       /* required: true*/
    },
     completed: {
        type: String
       /* required: true*/
    },
     status: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var Product = module.exports = mongoose.model('product_lists', productSchema);
var mongoose = require('mongoose');
// Setup schema
var TextSchema = mongoose.Schema({

    text_name: {
        type: String
        
    },
    arabic_text: {
        type: String
        
    },
    english_text :{
        type: String
    },
    segment_tag :{
        type: String
    }

   
});
// Export Contact model
var Texts = module.exports = mongoose.model('texts', TextSchema);
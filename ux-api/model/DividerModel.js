var mongoose = require('mongoose');
// Setup schema
var dividerSchema = mongoose.Schema({

    content_name: {
        type: String
        /*required: true*/
    },
    divider_type: {
        type: String
        /*required: true*/
    },
    element_name: {
        type: String
        /*required: true*/
    },
    segment_tag: {
        type: String
        /*required: true*/
    },
    element_content: {
        type: String
        /*required: true*/
    },
    page_id: {
        type: String
       /* required: true*/
    }   

});
// Export Contact model
var Divider = module.exports = mongoose.model('dividers', dividerSchema);
var mongoose = require('mongoose');
// Setup schema
var mapSchema = mongoose.Schema({

    location_title: {
        type: String        
    },
    location_name: {
        type: String        
    },
    location_description :{
        type: String
    },
    latitude :{
        type: String
    },
    longtitude :{
        type: String
    },
    map_url :{
        type: String
    },
    phone :{
        type: String
    },
    working_hours: String,
    segment_tag : String
   
});
// Export Contact model
var Maps = module.exports = mongoose.model('maps', mapSchema);
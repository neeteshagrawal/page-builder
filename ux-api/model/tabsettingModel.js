var mongoose = require('mongoose');
// Setup schema
var tabsettingSchema = mongoose.Schema({
    border: {
        type: String
        /*required: true*/
    },
    tab_position: {
        type: String
        /*required: true*/
    },    
    label: {
        type: String
       /* required: true*/
    },
    label_aligment: {
        type: String
       /* required: true*/
    },
    icon_position: {
        type: String
       /* required: true*/
    },
    active_btn_clr: {
        type: String
       /* required: true*/
    }, 
    active_lbl_clr: {
        type: String
       /* required: true*/
    }, 
    inactive_btn_clr: {
        type: String
       /* required: true*/
    }, 
    inactive_lbl_clr: {
        type: String
       /* required: true*/
    }, 
    border_clr: {
        type: String
       /* required: true*/
    },  
    background_clr: {
        type: String
       /* required: true*/
    } ,
    page_id: {
        type: String
       /* required: true*/
    }   ,
    post_type: {
        type: String
       /* required: true*/
    }  

});
// Export Contact model
var Tabsetting = module.exports = mongoose.model('tabsettings', tabsettingSchema);
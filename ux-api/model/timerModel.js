var mongoose = require('mongoose');
// Setup schema
var timerSchema = mongoose.Schema({
    start_date: {
        type: String
        /*required: true*/
    },
    end_date: {
        type: String
        /*required: true*/
    },
    segment: {
        type: String
       /* required: true*/
    },
    dynamic_logic: {
        type: String
       /* required: true*/
    }
     
    

});
// Export Contact model
var Timer = module.exports = mongoose.model('timers', timerSchema);
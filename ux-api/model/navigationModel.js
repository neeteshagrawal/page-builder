var mongoose = require('mongoose');
// Setup schema
var navigationSchema = mongoose.Schema({
    navigation_template: {
        type: String
        /*required: true*/
    },
    destination_name: {
        type: String
        /*required: true*/
    },
    destination_page: {
        type: String
       /* required: true*/
    },
    segment_tag: {
        type: String
       /* required: true*/
    },
     navigation_content1: {
        type: String
       /* required: true*/
    },
     navigation_content2: {
        type: String
       /* required: true*/
    },
     use_budget: {
        type: String
       /* required: true*/
    },
     budget_algo: {
        type: String
       /* required: true*/
    }
    

});
// Export Contact model
var Navigation = module.exports = mongoose.model('navigations', navigationSchema);
var mongoose = require('mongoose');
// Setup schema
var threeObjectSchema = mongoose.Schema({
    page_id: {
        type: String
        /*required: true*/
    },
    column_type: {
        type: String
        /*required: true*/
    },
    order: {
        type: String
       /* required: true*/
    }

});
// Export Contact model
var ThreeObject = module.exports = mongoose.model('threeObjects', threeObjectSchema);
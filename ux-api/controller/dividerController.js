// Import contact model
Divider = require('../model/DividerModel');
// Handle index actions

exports.findAll = (req, res) => {
    Divider.find()
    .then(divider => {
        res.json({
            status: "success",
            message: "Divider Data retrieved successfully",
            data: divider
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var divider = new Divider();
    console.log(req.body);
    divider.content_name = req.body.content_name;
    divider.divider_type = req.body.divider_type;
    divider.element_name = req.body.element_name;
    divider.segment_tag = req.body.segment_tag;
    divider.element_content = req.body.element_content;    
    divider.page_id = req.body.page_id;
   
    
// save the contact and check for errors
    divider.save(function (err) {

        if (err)
        res.json(err);
res.json({
            message: 'New Divider Data is Inserted!',
            data: divider
        });
    });
};


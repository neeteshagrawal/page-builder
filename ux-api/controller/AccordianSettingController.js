// Import Video model
Accordionsetting = require('../model/AccordionSettingModel');
// Handle index actions
exports.findAll = (req, res) => {
    Accordionsetting.find()
    .then(accordionsetting => {
        res.json({
            status: "success",
            message: "Accordian Setting retrieved successfully",
            data: accordionsetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var accordionsetting = new Accordionsetting();
    accordionsetting.border = req.body.border ? req.body.border : accordionsetting.border;
    accordionsetting.height = req.body.height;
    accordionsetting.width = req.body.width;
    accordionsetting.rounded_edge = req.body.rounded_edge;
    accordionsetting.accordian_icon = req.body.accordian_icon;
    accordionsetting.label = req.body.label;
    accordionsetting.label_aligment = req.body.label_aligment;
    accordionsetting.icon_position = req.body.icon_position;
    accordionsetting.active_btn_clr = req.body.active_btn_clr;
    accordionsetting.active_lbl_clr = req.body.active_lbl_clr;
    accordionsetting.inactive_btn_clr = req.body.inactive_btn_clr;
    accordionsetting.inactive_lbl_clr = req.body.inactive_lbl_clr;
    accordionsetting.border_clr = req.body.border_clr;
    accordionsetting.background_clr = req.body.background_clr;
    accordionsetting.page_id = req.body.page_id;
    
   
// save the Video and check for errors
    accordionsetting.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Accordian Setting  setting Added!',
            data: accordionsetting
        });
    });
};
// Handle view Video info
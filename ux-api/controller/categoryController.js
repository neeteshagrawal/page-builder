// Import product model
Category = require('../model/categoryModel');
// Handle index actions
exports.findAll = (req, res) => {
    Category.find()
    .then(category => {
        res.json({
            status: "success",
            message: "Category retrieved successfully",
            data: category
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


// Handle create product actions
exports.new = function (req, res) {
    var category = new Category();
    category.segment_tags = req.body.segment_tags ? req.body.segment_tags : product.segment_tags;
    category.category_level = req.body.category_level;
    category.media_column = req.body.media_column;
    category.no_of_column = req.body.no_of_column;
    category.no_of_row = req.body.no_of_row;
    category.text_size = req.body.text_size;
    category.text_color = req.body.text_color;
    category.text_bgcolor = req.body.text_bgcolor;
   
// save the product and check for errors
    category.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Category Added!',
            data: category
        });
    });
};
// Handle view product info

// Import Video model
Navigationsetting = require('../model/navigationsettingModel');
// Handle index actions
exports.findAll = (req, res) => {
    Navigationsetting.find()
    .then(navigationsetting => {
        res.json({
            status: "success",
            message: "Navigation setting retrieved successfully",
            data: navigationsetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var navigationsetting = new Navigationsetting();
    navigationsetting.boarder = req.body.boarder ? req.body.boarder : navigationsetting.boarder;
    navigationsetting.space = req.body.space;
    navigationsetting.use_badge = req.body.use_badge;
    navigationsetting.max_badge_count = req.body.max_badge_count;
    navigationsetting.label = req.body.label;
    navigationsetting.label_aligment = req.body.label_aligment;
    navigationsetting.icon_position = req.body.icon_position;
    navigationsetting.active_btn_clr = req.body.active_btn_clr;
    navigationsetting.active_lbl_clr = req.body.active_lbl_clr;
    navigationsetting.inactive_btn_clr = req.body.inactive_btn_clr;
    navigationsetting.inactive_lbl_clr = req.body.inactive_lbl_clr;
    navigationsetting.border_clr = req.body.border_clr;
    navigationsetting.background_clr = req.body.background_clr;
    navigationsetting.page_id = req.body.page_id;
    navigationsetting.post_type = req.body.post_type;
    
   
// save the Video and check for errors
    navigationsetting.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Navigation setting Added!',
            data: navigationsetting
        });
    });
};
// Handle view Video info

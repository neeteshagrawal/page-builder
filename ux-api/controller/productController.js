// Import product model
Product = require('../model/productModel');
// Handle index actions
exports.index = function (req, res) {
    product.get(function (err, products) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "products retrieved successfully",
            data: products
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var product = new Product();
    product.sku = req.body.sku ? req.body.sku : product.sku;
    product.name = req.body.name;
    product.brand_name = req.body.brand_name;
    product.price = req.body.price;
    product.qty = req.body.qty;
    product.type = req.body.type;
    product.attribute_group = req.body.attribute_group;
    product.completed = req.body.completed;
    product.qty = req.body.qty;
    product.status = req.body.status;
// save the product and check for errors
    product.save(function (err) {
        if (err)
            res.json(err);
res.json({
            message: 'New Product Added!',
            data: product
        });
    });
};
// Handle view product info

// Import contact model
Banner = require('../model/BannerModel');
// Handle index actions

exports.findAll = (req, res) => {
    Banner.find()
    .then(banner => {
        res.json({
            status: "success",
            message: "Banner retrieved successfully",
            data: banner
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var contact = new Banner();
    console.log(req.body);
    contact.autoplayspeed = req.body.autoplayspeed;
    contact.time = req.body.time;
    contact.banner_name = req.body.banner_name;
    contact.pages = req.body.pages;
    contact.segment_tag = req.body.segment_tag;
    contact.dynamic_logic = req.body.dynamic_logic;
    
// save the contact and check for errors
    contact.save(function (err) {

        if (err)
        res.json(err);
res.json({
            message: 'New Banner is created!',
            data: contact
        });
    });
};
// Handle view contact info
exports.view = function (req, res) {
    Contact.findById(req.params.contact_id, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            message: 'Contact details loading..',
            data: contact
        });
    });
};
// Handle update contact info
exports.update = function (req, res) {
Contact.findById(req.params.contact_id, function (err, contact) {
        if (err)
            res.send(err);
contact.name = req.body.name ? req.body.name : contact.name;
        contact.gender = req.body.gender;
        contact.email = req.body.email;
        contact.phone = req.body.phone;
// save the contact and check for errors
        contact.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Contact Info updated',
                data: contact
            });
        });
    });
};
// Handle delete contact
exports.delete = function (req, res) {
    Contact.remove({
        _id: req.params.contact_id
    }, function (err, contact) {
        if (err)
            res.send(err);
res.json({
            status: "success",
            message: 'Contact deleted'
        });
    });
};
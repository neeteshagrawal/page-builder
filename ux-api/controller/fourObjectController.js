// Import product model
FourObject = require('../model/fourObjectModel');
// Handle index actions
exports.findAll = (req, res) => {
    FourObject.find()
    .then(fourObject => {
        res.json({
            status: "success",
            message: "FourObject retrieved successfully",
            data: fourObject
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var fourObject = new FourObject();
    fourObject.page_id = req.body.page_id ? req.body.page_id : fourObject.page_id;    
    fourObject.order = req.body.order;
    fourObject.column_type = req.body.column_type;
    
   
// save the product and check for errors
    fourObject.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Four Object Added!',
            data: fourObject
        });
    });
};
// Handle view product info

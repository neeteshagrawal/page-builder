// Import contact model
Maps = require('../model/MapModel');
// Handle index actions
exports.findAll = (req, res) => {
    Maps.find()
    .then(maps=> {
        res.json({
            status: "success",
            message: "Maps retrieved successfully",
            data: maps
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var map = new Maps();  
    map.location_title = req.body.location_title;
    map.location_name = req.body.location_name;
    map.location_description = req.body.location_description;
    map.latitude = req.body.latitude;
    map.longtitude = req.body.longtitude;
    map.map_url = req.body.map_url;
    map.phone = req.body.phone;
    map.working_hours = req.body.working_hours;
    map.segment_tag = req.body.segment_tag;
    
// save the contact and check for errors
    map.save(function (err) {

        if (err)
        res.json(err);
        res.json({
            message: 'New Map is created!',
            data: map
        });
    });
};
// Handle view contact info

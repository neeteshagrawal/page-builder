// Import Video model
Tabsetting = require('../model/tabsettingModel');
// Handle index actions
exports.findAll = (req, res) => {
    Tabsetting.find()
    .then(tabsetting=> {
        res.json({
            status: "success",
            message: "Tab setting retrieved successfully",
            data: tabsetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var tabsetting = new Tabsetting();
    tabsetting.border = req.body.border ? req.body.border : tabsetting.border;
    tabsetting.tab_position = req.body.tab_position;  
    tabsetting.label = req.body.label;
    tabsetting.label_aligment = req.body.label_aligment;
    tabsetting.icon_position = req.body.icon_position;
    tabsetting.active_btn_clr = req.body.active_btn_clr;
    tabsetting.active_lbl_clr = req.body.active_lbl_clr;
    tabsetting.inactive_btn_clr = req.body.inactive_btn_clr;
    tabsetting.inactive_lbl_clr = req.body.inactive_lbl_clr;
    tabsetting.border_clr = req.body.border_clr;
    tabsetting.background_clr = req.body.background_clr;
    tabsetting.page_id = req.body.page_id;
    tabsetting.post_type = req.body.post_type;
    
   
// save the Video and check for errors
    tabsetting.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Tab setting Added!',
            data: tabsetting
        });
    });
};
// Handle view Video info

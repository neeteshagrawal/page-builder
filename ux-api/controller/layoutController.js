// Import Video model
Layout = require('../model/layoutModel');
// Handle index actions
exports.findAll = (req, res) => {
    Layout.find()
    .then(layout => {
        res.json({
            status: "success",
            message: "Layout retrieved successfully",
            data: layout
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var layout = new Layout();
    layout.static_banner = req.body.static_banner ? req.body.static_banner : layout.static_banner;
    layout.round_edge = req.body.round_edge;
    layout.overlay_color = req.body.overlay_color;
    layout.opacity = req.body.opacity;
    layout.color = req.body.color;
    layout.content_margin = req.body.content_margin;
    layout.element_color = req.body.element_color;
    layout.content_color = req.body.content_color;
    layout.main_color = req.body.main_color;
    layout.sub_color = req.body.sub_color;
    layout.page_id = req.body.page_id;
    
   
// save the Video and check for errors
    layout.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Layout Added!',
            data: layout
        });
    });
};
// Handle view Video info

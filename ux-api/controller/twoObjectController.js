// Import product model
TwoObject = require('../model/twoObjectModel');
// Handle index actions
exports.findAll = (req, res) => {
    TwoObject.find()
    .then(twoObject => {
        res.json({
            status: "success",
            message: "Two Object retrieved successfully",
            data: twoObject
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var twoObject = new TwoObject();
    twoObject.page_id = req.body.page_id ? req.body.page_id : twoObject.page_id;    
    twoObject.order = req.body.order;
    twoObject.column_type = req.body.column_type;
    
   
// save the product and check for errors
    twoObject.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New two Object Added!',
            data: twoObject
        });
    });
};
// Handle view product info

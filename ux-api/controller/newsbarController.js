// Import Video model
Newsbar = require('../model/newsbarModel');
// Handle index actions
exports.findAll = (req, res) => {
    Newsbar.find()
    .then(newsbar => {
        res.json({
            status: "success",
            message: "Newsbar retrieved successfully",
            data: newsbar
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var newsbar = new Newsbar();
    newsbar.news_speed = req.body.news_speed ? req.body.news_speed : newsbar.news_speed;
    newsbar.bg_color = req.body.bg_color;
    newsbar.message_name = req.body.message_name;
    newsbar.segment_tags = req.body.segment_tags;
    newsbar.message_content1 = req.body.message_content1;
    newsbar.message_content2 = req.body.message_content2;
    
   
// save the Video and check for errors
    newsbar.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New News bar Added!',
            data: newsbar
        });
    });
};
// Handle view Video info

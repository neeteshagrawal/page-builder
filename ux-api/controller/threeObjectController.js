// Import product model
ThreeObject = require('../model/threeObjectModel');
// Handle index actions
exports.findAll = (req, res) => {
    ThreeObject.find()
    .then(threeObject => {
        res.json({
            status: "success",
            message: "Three Object retrieved successfully",
            data: threeObject
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var threeObject = new ThreeObject();
    threeObject.page_id = req.body.page_id ? req.body.page_id : threeObject.page_id;    
    threeObject.order = req.body.order;
    threeObject.column_type = req.body.column_type;
    
   
// save the product and check for errors
    threeObject.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Three Object Added!',
            data: threeObject
        });
    });
};
// Handle view product info

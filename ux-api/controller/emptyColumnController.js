// Import product model
EmptyColumn = require('../model/emptyColumnModel');
// Handle index actions
exports.findAll = (req, res) => {
    EmptyColumn.find()
    .then(emptyColumn => {
        res.json({
            status: "success",
            message: "Empty Column retrieved successfully",
            data: emptyColumn
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var emptyColumn = new EmptyColumn();
    emptyColumn.page_id = req.body.page_id ? req.body.page_id : emptyColumn.page_id;    
    emptyColumn.order = req.body.order;
    emptyColumn.column_number = req.body.column_number;
    
   
// save the product and check for errors
    emptyColumn.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Empty Column Added!',
            data: emptyColumn
        });
    });
};
// Handle view product info

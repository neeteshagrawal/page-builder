// Import Tabs model
Tab = require('../model/TabModel');
// Handle index actions
exports.findAll = (req, res) => {
    Tab.find()
    .then(tab=> {
        res.json({
            status: "success",
            message: "Tab retrieved successfully",
            data: tab
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Tabs actions
exports.new = function (req, res) {
    var tab = new Tab();
    tab.tab_name = req.body.tab_name;
    tab.element_name = req.body.element_name;
    tab.tab_content = req.body.tab_content;
    tab.segment_tag = req.body.segment_tag;
// save the contact and check for errors
    tab.save(function (err) {

        if (err)
        res.json(err);
        res.json({
            message: 'New Tabs is created!',
            data: tab
        });
    });
};

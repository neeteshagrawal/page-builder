// Import contact model
Texts = require('../model/TextModel');
// Handle index actions

exports.findAll = (req, res) => {
    Texts.find()
    .then(text => {
        res.json({
            status: "success",
            message: "Text retrieved successfully",
            data: text
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var texts = new Texts();
    console.log(req.body);
    texts.text_name = req.body.text_name;
    texts.arabic_text = req.body.arabic_text;
    texts.english_text = req.body.english_text;
    texts.segment_tag = req.body.segment_tag;
// save the contact and check for errors
    texts.save(function (err) {

        if (err)
        res.json(err);
res.json({
            message: 'New text is created!',
            data: texts
        });
    });
};

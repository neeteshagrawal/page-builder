// Import contact model
Textsetting = require('../model/TextSettingModel');
// Handle index actions


exports.findAll = (req, res) => {
    Textsetting.find()
    .then(textsetting => {
        res.json({
            status: "success",
            message: "Text setting retrieved successfully",
            data: textsetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var textsetting = new Textsetting();
    console.log(req.body);
    textsetting.english_font = req.body.english_font;
    textsetting.arabic_font = req.body.arabic_font;
    textsetting.english_font_type = req.body.english_font_type;
    textsetting.arabic_font_type = req.body.arabic_font_type;
    textsetting.english_font_size = req.body.english_font_size;
    textsetting.arabic_font_size = req.body.arabic_font_size;
// save the contact and check for errors
    textsetting.save(function (err) {

        if (err)
        res.json(err);
res.json({
            message: 'New textsetting is Inserted!',
            data: textsetting
        });
    });
};

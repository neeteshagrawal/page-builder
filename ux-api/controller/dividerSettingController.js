// Import DividerSetting model
Dividersetting = require('../model/DividerSettingModel');

// Handle findAll actions
exports.findAll = (req, res) => {
    Dividersetting.find()
    .then(dividersetting => {
        res.json({
            status: "success",
            message: "Divider Setting retrieved successfully",
            data: dividersetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var dividersetting = new Dividersetting();
   
    dividersetting.line_width = req.body.line_width;
    dividersetting.line_thickness = req.body.line_thickness;
    dividersetting.add_element = req.body.add_element;
    dividersetting.element_position = req.body.element_position;
    dividersetting.line_thickness_color = req.body.line_thickness_color;
    dividersetting.element_color = req.body.element_color;
    dividersetting.element_background_color = req.body.element_background_color;
    dividersetting.page_id = req.body.page_id;
    
   
// save the Video and check for errors
    dividersetting.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Divider setting Added!',
            data: dividersetting
        });
    });
};
// Handle view Video info
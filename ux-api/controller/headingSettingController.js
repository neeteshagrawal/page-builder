// Import contact model
Headingsetting = require('../model/headingSettingModel');
// Handle index actions


exports.findAll = (req, res) => {
    Headingsetting.find()
    .then(headingsetting => {
        res.json({
            status: "success",
            message: "Heading Setting retrieved successfully",
            data: headingsetting
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create contact actions
//console.log("neetesh");
exports.new = function (req, res) {
    var headingsetting = new Headingsetting();
    headingsetting.english_font = req.body.english_font;
    headingsetting.arabic_font = req.body.arabic_font;
    headingsetting.english_font_type = req.body.english_font_type;
    headingsetting.arabic_font_type = req.body.arabic_font_type;
    headingsetting.english_font_size = req.body.english_font_size;
    headingsetting.arabic_font_size = req.body.arabic_font_size;
// save the contact and check for errors
    headingsetting.save(function (err) {

        if (err)
        res.json(err);
        res.json({
            message: 'New Heading Setting is created!',
            data: headingsetting
        });
    });
};

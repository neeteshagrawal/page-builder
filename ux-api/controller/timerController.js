// Import product model
Timer = require('../model/timerModel');
// Handle index actions
exports.findAll = (req, res) => {
    Timer.find()
    .then(timer => {
        res.json({
            status: "success",
            message: "Timer retrieved successfully",
            data: timer
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var timer = new Timer();
    timer.start_date = req.body.start_date ? req.body.start_date : timer.start_date;
    timer.end_date = req.body.end_date;
    timer.segment = req.body.segment;
    timer.dynamic_logic = req.body.dynamic_logic;
    
   
// save the product and check for errors
    timer.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Timer Added!',
            data: timer
        });
    });
};
// Handle view product info

// Import Video model
Video = require('../model/videoModel');
// Handle index actions
exports.findAll = (req, res) => {
    Video.find()
    .then(video => {
        res.json({
            status: "success",
            message: "Video retrieved successfully",
            data: video
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create Video actions
exports.new = function (req, res) {
    var video = new Video();
    video.video_name = req.body.video_name ? req.body.video_name : video.video_name;
    video.segment_tag = req.body.segment_tag;
    video.auto_play = req.body.auto_play;
    video.controls = req.body.controls;
    video.muted = req.body.muted;
    video.video1 = req.body.video1;
    video.video2 = req.body.video2;
    
   
// save the Video and check for errors
    video.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Video Added!',
            data: video
        });
    });
};
// Handle view Video info

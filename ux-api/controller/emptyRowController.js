// Import product model
EmptyRow = require('../model/emptyRowModel');
// Handle index actions
exports.findAll = (req, res) => {
    EmptyRow.find()
    .then(emptyRow => {
        res.json({
            status: "success",
            message: "Empty Row retrieved successfully",
            data: emptyRow
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create product actions
exports.new = function (req, res) {
    var emptyRow = new EmptyRow();
    emptyRow.page_id = req.body.page_id ? req.body.page_id : emptyRow.page_id;
    emptyRow.empty_row = req.body.empty_row;
    emptyRow.order = req.body.order;
    
   
// save the product and check for errors
    emptyRow.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Empty Row Added!',
            data: emptyRow
        });
    });
};
// Handle view product info

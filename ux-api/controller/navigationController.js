// Import navigation model
Navigation = require('../model/navigationModel');
// Handle index actions
exports.findAll = (req, res) => {
    Navigation.find()
    .then(navigation => {
        res.json({
            status: "success",
            message: "Navigation retrieved successfully",
            data: navigation
        });
        //res.send(accordian);
        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Handle create navigation actions
exports.new = function (req, res) {
    var navigation = new Navigation();
    navigation.navigation_template = req.body.navigation_template ? req.body.navigation_template : navigation.navigation_template;
    navigation.destination_name = req.body.destination_name;
    navigation.destination_page = req.body.destination_page;
    navigation.segment_tag = req.body.segment_tag;
    navigation.navigation_content1 = req.body.navigation_content1;
    navigation.navigation_content2 = req.body.navigation_content2;
    navigation.use_budget = req.body.use_budget;
    navigation.budget_algo = req.body.budget_algo;
   
// save the navigation and check for errors
    navigation.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New Navigation Added!',
            data: navigation
        });
    });
};
// Handle view navigation info

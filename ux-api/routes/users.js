var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
	res.json({
		status: 'API Its Working',
		message: 'Welcome to Page Builder API!'
	});
});

module.exports = router;

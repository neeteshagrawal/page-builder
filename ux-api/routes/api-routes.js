// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Import contact controller
//var contactController = require('../contactController');
var productController = require('../controller/productController');
var categoryController = require('../controller/categoryController');
var timerController = require('../controller/timerController');
var videoController = require('../controller/videoController');
var newsbarController = require('../controller/newsbarController');
var navigationsettingController = require('../controller/navigationsettingController');
var navigationController = require('../controller/navigationController');
var layoutController = require('../controller/layoutController');
var emptyRowController = require('../controller/emptyRowController');
var emptyColumnController = require('../controller/emptyColumnController');
var twoObjectController = require('../controller/twoObjectController');
var threeObjectController = require('../controller/threeObjectController');
var fourObjectController = require('../controller/fourObjectController');
var tabsettingController = require('../controller/tabsettingController');
//N
var bannerController = require('../controller/bannerController');
var mapController = require('../controller/mapController');
var textController = require('../controller/textController');
var tabController = require('../controller/tabController');
var dividerController = require('../controller/dividerController');
var dividerSettingController = require('../controller/dividerSettingController');
var htmlCodeController = require('../controller/htmlCodeController');
var accordionController = require('../controller/accordionController');
var accordionSettingController = require('../controller/AccordianSettingController');
var headingSettingController = require('../controller/headingSettingController');
var textSettingController = require('../controller/textSettingController');
// Contact routes
router.route('/product')
    .get(productController.index)
    .post(productController.new);
/*router.route('/contacts/:contact_id')
    .get(contactController.view)
    .patch(contactController.update)
    .put(contactController.update)
    .delete(contactController.delete);*/
router.route('/category')
    .get(categoryController.findAll)
    .post(categoryController.new);
router.route('/timer')
    .get(timerController.findAll)
    .post(timerController.new);
router.route('/video')
    .get(videoController.findAll)
    .post(videoController.new);
router.route('/newsbar')
    .get(newsbarController.findAll)
    .post(newsbarController.new);
router.route('/navigationsetting')
    .get(navigationsettingController.findAll)
    .post(navigationsettingController.new);
router.route('/navigation')
    .get(navigationController.findAll)
    .post(navigationController.new);
router.route('/layout')
    .get(layoutController.findAll)
    .post(layoutController.new); 
router.route('/emptyRow')
    .get(emptyRowController.findAll)
    .post(emptyRowController.new);  
router.route('/emptyColumn')
    .get(emptyColumnController.findAll)
    .post(emptyColumnController.new);   
router.route('/twoObject')
    .get(twoObjectController.findAll)
    .post(twoObjectController.new);  
router.route('/threeObject')
    .get(threeObjectController.findAll)
    .post(threeObjectController.new);    
router.route('/fourObject')
    .get(fourObjectController.findAll)
    .post(fourObjectController.new); 
router.route('/tabsetting')
    .get(tabsettingController.findAll)
    .post(tabsettingController.new); 

router.route('/banners')
    //.get(bannerController.index)
    .post(bannerController.new);
router.get('/banners', bannerController.findAll);  
router.route('/maps')
    .get(mapController.findAll)
    .post(mapController.new);  
router.route('/text')
    .get(textController.findAll)
    .post(textController.new);
router.route('/tabs')
    .get(tabController.findAll)
    .post(tabController.new);
router.route('/SaveDividerDetails')
  .post(dividerController.new);
router.get('/GetDividerDetails', dividerController.findAll);

router.route('/SaveDividerSetting')
  .post(dividerSettingController.new);
router.route('/GetDividerSetting')
    .get(dividerSettingController.findAll);
router.route('/htmlCode')
    .get(htmlCodeController.findAll)
    .post(htmlCodeController.new);

router.route('/accordian')
  .post(accordionController.new);
router.get('/accordian', accordionController.findAll);
router.route('/accordianSetting')
  .post(accordionSettingController.new);
router.route('/getAccordianSetting')
  .get(accordionSettingController.findAll);
router.route('/headingSetting')
  .get(headingSettingController.findAll);
router.route('/headingSetting')
  .post(headingSettingController.new);

router.route('/textSetting')
    .get(textSettingController.findAll)
    .post(textSettingController.new);

// Export API routes
module.exports = router;